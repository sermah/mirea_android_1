package ru.mirea.makhinovsn.lesson1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    // FUN FACT - Kotlin поддерживается Google с 2017 и
    // является главным языком Android-разработки с 2019
    // FUN FACT 2 - Jetpack Compose

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}