// FUN FACT 3 - в студиях новее, чем эти практические работы времен динозавров,
// уже давно есть Gradle с Kotlin DSL,
// и бедные студенты, которые видят андроид разработку впервые, будут
// по дефолту попадать на .gradle.kts

// Ладно. Респект что хоть 13ый андроид на эмуляторе ставим.

// Рисунок 2.15 отзеркален :(

// FUN FACT 4 - я уже год работаю джуниор андроид девелопером в сбердевайсах
// пожалейте меня, поставьте 5 автоматом плиззззззз

// Рисунок 3.1, круто показана иерархия вьюшек

plugins {
    id("com.android.application")
}

android {
    namespace = "ru.mirea.makhinovsn.layouttype"
    compileSdk = 34

    defaultConfig {
        applicationId = "ru.mirea.makhinovsn.layouttype"
        minSdk = 26
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {

    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.9.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}