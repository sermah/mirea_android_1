package ru.mirea.makhinovsn.buttonclicker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView tvOut;
    private Button btnWhoAmI;
    private Button btnItsNotMe;
    private CheckBox checkBoxik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvOut = findViewById(R.id.tvOut);
        btnWhoAmI = findViewById(R.id.btnWhoAmI);
        btnItsNotMe = findViewById(R.id.btnItsNotMe);
        checkBoxik = findViewById(R.id.checkBoxik);

        btnWhoAmI.setOnClickListener(view -> { // в 2024 живем какбэ
            tvOut.setText("Мой номер по списку №14");
            checkBoxik.setChecked(true);
        });
    }

    // Держу в курсе, onClick никто не использует и
    // вообще даже в доке пишется что надо листенер ставить
    public void onBtnItsNotMeClick(View view) {
        Toast.makeText(this, "Это не я типа", Toast.LENGTH_SHORT).show();
        checkBoxik.setChecked(false);
    }
}